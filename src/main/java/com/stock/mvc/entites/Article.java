package com.stock.mvc.entites;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name = "article")
public class Article  implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name = "Id_Article")
	private Long IdArticle;
	private String codeArticle;
	private String designation;
	private BigDecimal prixUnitairHT;
	private BigDecimal tauxtva;
	private BigDecimal prixUnitairTTC;
	private String photo;
	
	@ManyToOne
	@JoinColumn(name = "idCategory")
	private Category category;
	
	
	public Article() {
		super();
	}

	public Long getIdArticle() {
		return IdArticle;
	}

	public void setIdArticle(Long idArticle) {
		IdArticle = idArticle;
	}

	public String getCodeArticle() {
		return codeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		this.codeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixUnitairHT() {
		return prixUnitairHT;
	}

	public void setPrixUnitairHT(BigDecimal prixUnitairHT) {
		this.prixUnitairHT = prixUnitairHT;
	}

	public BigDecimal getTauxtva() {
		return tauxtva;
	}

	public void setTauxtva(BigDecimal tauxtva) {
		this.tauxtva = tauxtva;
	}

	public BigDecimal getPrixUnitairTTC() {
		return prixUnitairTTC;
	}

	public void setPrixUnitairTTC(BigDecimal prixUnitairTTC) {
		this.prixUnitairTTC = prixUnitairTTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	
	
	

	
	
}
